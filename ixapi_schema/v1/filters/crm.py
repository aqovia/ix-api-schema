
"""
Default Filters for CRM objects
"""

from ixapi_schema.openapi import components

OWNABLE = {
    "managing_customer": components.CharFilter(),
    "consuming_customer": components.CharFilter(),
    "external_ref": components.CharFilter(),
}


INVOICEABLE = {
    "purchase_order": components.CharFilter(),
    "contract_ref": components.CharFilter(),
}


