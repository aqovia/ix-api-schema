
import enum


class ConnectionMode(enum.Enum):
    """The mode of a connection"""
    MODE_LACP = "lag_lacp"
    MODE_STATIC = "lag_static"
    MODE_FLEX_ETHERNET = "flex_ethernet"
    MODE_STANDALONE = "standalone"


class BGPSessionType(enum.Enum):
    """BGP session type"""
    TYPE_ACTIVE = "active"
    TYPE_PASSIVE = "passive"


class RouteServerSessionMode(enum.Enum):
    """RouteServer session mode"""
    MODE_PUBLIC = "public"
    MODE_COLLECTOR = "collector"


class LACPTimeout(enum.Enum):
    """The LACP timeout"""
    TIMEOUT_SLOW = "slow"
    TIMEOUT_FAST = "fast"


#
# Config Types
#
NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN = "exchange_lan"
NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP = "closed_user_group"
NETWORK_SERVICE_CONFIG_TYPE_CLOUD = "cloud"
NETWORK_SERVICE_CONFIG_TYPE_ELINE = "eline"

NETWORK_SERVICE_CONFIG_TYPES = [
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP,
    NETWORK_SERVICE_CONFIG_TYPE_ELINE,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
]

NETWORK_SERVICE_CONFIG_ENTITIES = {
    "access.ExchangeLanNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    "access.ClosedUserGroupNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP,
    "access.ELineNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_ELINE,
    "access.CloudNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
}

NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING = "blackholing"
NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER = "route_server"
NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER = "ixp_router"

NETWORK_FEATURE_CONFIG_TYPES = [
    NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING,
    NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
    NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER,
]

NETWORK_FEATURE_CONFIG_ENTITIES = {
    "access.BlackholingNetworkFeatureConfig":
        NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING,
    "access.RouteServerNetworkFeatureConfig":
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
    "access.IXPRouterNetworkFeatureConfig":
        NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER,
}

