
"""
Service Endpoints
"""

from ixapi_schema.openapi import components
from ixapi_schema.v2 import filters
from ixapi_schema.v2.entities import service, problems


PATHS = {
    #
    # Network Services
    #
    "/network-services": {
        "description": """
            A `NetworkService` is an instances of a `Product` accessible by one
            or multiple users, depending on the type of product

            For example, each Exchange Network LAN is considered as a shared
            instance of the related LAN's `Product`
        """,
        "GET": {
            "description": """
                List available `network-services`.
            """,
            "responses": {
                200: service.NetworkService(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.OWNABLE,
                {
                    "type": components.ChoiceFilter(
                        service.NETWORK_SERVICE_TYPES),
                    "pop": components.CharFilter(),
                    "product": components.CharFilter(),
                },
            ),
        },
        "POST": {
            "description": """
                Create a new network service
            """,
            "request": service.NetworkServiceRequest(),
            "responses": {
                201: service.NetworkService(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        }
    },
    "/network-services/<id:str>": {
        "description": """
            A `NetworkService` is an instances of a `Product` accessible by one
            or multiple users, depending on the type of product

            For example, each Exchange Network LAN is considered as a shared
            instance of the related LAN's `Product`
        """,
        "GET": {
            "description": """
                Get a specific `network-service` by id.
            """,
            "responses": {
                200: service.NetworkService(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """Update a network service""",
            "request": service.NetworkServiceRequest(),
            "responses": {
                200: service.NetworkService(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """Partially update a network service""",
            "request": service.NetworkServiceRequest(partial=True),
            "responses": {
                200: service.NetworkService(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
    },

    #
    # Network Features
    #
    "/network-features": {
        "description": """
            `NetworkFeatures` are functionality made available to customers
            within a `NetworkService`.
            Certain features may need to be configured by a customer to use
            that service.

            This can be for example a `route server` on an `exchange lan`.

            Some of these features are mandatory to configure if you
            want to access the platform. Which of these features you have to
            configure you can query using:
                `/api/v2/network-features?required=true`
        """,
        "GET": {
            "description": """
                List available network features.
            """,
            "responses": {
                200: service.NetworkFeature(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                {
                    "type": components.ChoiceFilter(
                        service.NETWORK_FEATURE_TYPES),
                    "required": components.CharFilter(),
                    "network_service": components.CharFilter(),
                    "name": components.CharFilter(),
                },
            ),
        },
    },
    "/network-features/<id:str>": {
        "description": """
            `NetworkFeatures` are functionality made available to customers
            within a `NetworkService`.
            Certain features may need to be configured by a customer to use
            that service.

            This can be for example a `route server` on an `exchange lan`.

            Some of these features are mandatory to configure if you
            want to access the platform. Which of these features you have to
            configure you can query using:
                `/api/v2/network-features?required=true`
        """,
        "GET": {
            "description": """
                Get a single network feature by it's id.
            """,
            "responses": {
                200: service.NetworkFeature(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    "/member-joining-rules": {
        "description": """
            This customer is allowed to see the network service
            and is able to join by creating network service configs.
        """,
        "GET": {
            "description": """Get a list of vlan members""",
            "filters": components.FilterSet(
                filters.COLLECTION,
                {
                    "network_service": components.CharFilter(),
                },
            ),
            "responses": {
                200: service.MemberJoiningRule(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
        },
        "POST": {
            "description": """Create a vlan membership""",
            "request": service.MemberJoiningRuleRequest(),
            "responses": {
                201: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        }
    },
    "/member-joining-rules/<id:str>": {
        "description": """
            Show or modify membership rules
        """,
        "GET": {
            "description": """
                Get a single rule
            """,
            "responses": {
                200: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PATCH": {
            "description": """Partially update a joining rule""",
            "request": service.MemberJoiningRuleUpdate(partial=True),
            "responses": {
                200: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PUT": {
            "description": """Update a joining rule""",
            "request": service.MemberJoiningRuleUpdate(),
            "responses": {
                200: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """Delete a joining rule""",
            "responses": {
                200: service.MemberJoiningRule(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        }
    }
}

