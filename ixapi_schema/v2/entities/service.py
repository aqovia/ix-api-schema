"""
Service Entities
----------------

"""

from ixapi_schema.openapi import components
from ixapi_schema.coupling import qs
from ixapi_schema.v2.entities import crm, events
from ixapi_schema.v2.constants.ipam import AddressFamilies
from ixapi_schema.v2.constants.service import (
    NETWORK_FEATURE_TYPE_BLACKHOLING,
    NETWORK_FEATURE_TYPE_ROUTESERVER,
    NETWORK_FEATURE_TYPE_IXPROUTER,
    NETWORK_FEATURE_TYPES,
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_E_LINE,
    NETWORK_SERVICE_TYPE_E_TREE,
    NETWORK_SERVICE_TYPE_E_LAN,
    NETWORK_SERVICE_TYPES,
    MEMBER_JOINING_RULE_TYPE_WHITELIST,
    MEMBER_JOINING_RULE_TYPE_BLACKLIST,
)
from ixapi_schema.v2.constants.access import (
    RouteServerSessionMode,
    BGPSessionType,
)


class IXPSpecificFeatureFlag(components.Component):
    """IXP-Specific Feature Flag"""
    name = components.CharField(
        max_length=20,
        help_text="""
            The name of the feature flag.

            Example: RPKI
        """)
    description = components.CharField(
        max_length=80,
        help_text="""
            The description of the feature flag.

            Example: RPKI Hard Filtering is available
        """)


class NetworkFeatureBase(
        components.Component,
    ):
    """Feature Base"""
    id = components.CharField()

    name = components.CharField(max_length=80)
    required = components.BooleanField()

    nfc_required_contact_roles = components.ListField(
        source="all_nfc_required_contact_roles",
        child=components.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified roles assigned to any associated contacts.

            The contacts can be associated through the `contacts` list
            property of the network feature configuration.
        """)

    flags = IXPSpecificFeatureFlag(
        source="ixp_specific_flags",
        many=True,
        help_text="""
            A list of IXP specific feature flags. This can be used
            to see if e.g. RPKI hard filtering is available.
        """)

    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"))


class BlackholingNetworkFeature(NetworkFeatureBase):
    """Blackholing Network Feature"""
    # Includes only base fields
    __polymorphic_type__ = \
        NETWORK_FEATURE_TYPE_BLACKHOLING
    __hidden_in_docs__ = True


class RouteServerNetworkFeature(NetworkFeatureBase):
    """Route Server Network Feature"""
    asn = components.IntegerField(min_value=0)
    fqdn = components.CharField(
        max_length=80,
        help_text="""
            The FQDN of the route server.

            Example: rs1.moon-ix.net
        """)
    looking_glass_url = components.CharField(
        help_text="""
            The url of the looking glass.

            Example: https://lg.moon-ix.net/rs1
        """)

    address_families = components.ListField(
        child=components.EnumField(AddressFamilies),
        min_length=1,
        max_length=2,
        help_text="""
            When creating a route server feature config, remember
            to specify which address family or families to use.

            Example: ["af_inet"]
        """)

    session_mode = components.EnumField(
        RouteServerSessionMode,
        help_text="""
            When creating a route server feature config, remember
            to specify the same session_mode as the route server.

            Example: "public"
        """)

    available_bgp_session_types = components.ListField(
        child=components.EnumField(BGPSessionType),
        min_length=1,
        max_length=2,
        help_text="""
            The route server provides the following session modes.

            Example: ["passive"]
        """)

    ips = components.PrimaryKeyRelatedField(
        source="ip_addresses",
        many=True,
        read_only=True)

    __polymorphic_type__ = \
        NETWORK_FEATURE_TYPE_ROUTESERVER


class IXPRouterNetworkFeature(NetworkFeatureBase):
    """IXP Router Network Feature"""
    asn = components.IntegerField(min_value=0)
    fqdn = components.CharField(max_length=80)

    ips = components.PrimaryKeyRelatedField(
        source="ip_addresses",
        read_only=True,
        many=True)

    __polymorphic_type__ = \
        NETWORK_FEATURE_TYPE_IXPROUTER
    __hidden_in_docs__ = True



class NetworkFeature(components.PolymorphicComponent):
    """Polymorphic Network Feature"""
    serializer_classes = {
        NETWORK_FEATURE_TYPE_BLACKHOLING:
            BlackholingNetworkFeature,
        NETWORK_FEATURE_TYPE_ROUTESERVER:
            RouteServerNetworkFeature,
        NETWORK_FEATURE_TYPE_IXPROUTER:
            IXPRouterNetworkFeature,
    }

    entity_types = {
        "service.BlackholingNetworkFeature":
            NETWORK_FEATURE_TYPE_BLACKHOLING,
        "service.RouteserverNetworkFeature":
            NETWORK_FEATURE_TYPE_ROUTESERVER,
        "service.IXPRouterNetworkFeature":
            NETWORK_FEATURE_TYPE_IXPROUTER,
    }


class MemberJoiningRuleBase(
        crm.Ownable,
        components.Component,
    ):
    """A rule for members joining a private vlan"""


class WhitelistMemberJoiningRuleBase(MemberJoiningRuleBase):
    """ELan Membership"""
    capacity_min = components.IntegerField(
        min_value=1,
        allow_null=True)
    capacity_max = components.IntegerField(
        min_value=1,
        allow_null=True)

    __polymorphic_type__ = \
        MEMBER_JOINING_RULE_TYPE_WHITELIST


class WhitelistMemberJoiningRule(WhitelistMemberJoiningRuleBase):
    """A rule for members joining a private vlan"""
    id = components.CharField()
    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"))


class WhitelistMemberJoiningRuleRequest(WhitelistMemberJoiningRuleBase):
    """A new vlan member joining rule"""
    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"))


class WhitelistMemberJoiningRuleUpdate(WhitelistMemberJoiningRuleBase):
    """A vlan member joining rule update"""
    # We do not allow changing the network service
    # of a member joining rule.


class BlacklistMemberJoiningRuleBase(MemberJoiningRuleBase):
    """ETree Membership"""
    __polymorphic_type__ = \
        MEMBER_JOINING_RULE_TYPE_BLACKLIST


class BlacklistMemberJoiningRule(BlacklistMemberJoiningRuleBase):
    """A rule for members joining a private vlan"""
    id = components.CharField()
    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"))


class BlacklistMemberJoiningRuleRequest(BlacklistMemberJoiningRuleBase):
    """A new vlan member joining rule"""
    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"))


class BlacklistMemberJoiningRuleUpdate(BlacklistMemberJoiningRuleBase):
    """A vlan member joining rule update"""
    # We do not allow changing the network service
    # of a member joining rule.


class MemberJoiningRule(components.PolymorphicComponent):
    """Polymorphic Member Joining Rule"""
    serializer_classes = {
        MEMBER_JOINING_RULE_TYPE_WHITELIST:
            WhitelistMemberJoiningRule,
        MEMBER_JOINING_RULE_TYPE_BLACKLIST:
            BlacklistMemberJoiningRule,
    }

    entity_types = {
        "service.WhitelistMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_WHITELIST,
        "service.BlacklistMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_BLACKLIST,
    }


class MemberJoiningRuleRequest(components.PolymorphicComponent):
    """Polymorphic Member Joining Rule Request"""
    serializer_classes = {
        MEMBER_JOINING_RULE_TYPE_WHITELIST:
            WhitelistMemberJoiningRuleRequest,
        MEMBER_JOINING_RULE_TYPE_BLACKLIST:
            BlacklistMemberJoiningRuleRequest,
    }

    entity_types = {
        "service.WhitelistMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_WHITELIST,
        "service.BlacklistMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_BLACKLIST,
    }


class MemberJoiningRuleUpdate(components.PolymorphicComponent):
    """Polymorphic Member Joining Rule Update"""
    serializer_classes = {
        MEMBER_JOINING_RULE_TYPE_WHITELIST:
            WhitelistMemberJoiningRuleUpdate,
        MEMBER_JOINING_RULE_TYPE_BLACKLIST:
            BlacklistMemberJoiningRuleUpdate,
    }

    entity_types = {
        "service.WhitelistMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_WHITELIST,
        "service.BlacklistMemberJoiningRule":
            MEMBER_JOINING_RULE_TYPE_BLACKLIST,
    }



#
# Network Services
#
class NetworkServiceBase(components.Component):
    """Network Service (Base)"""
    product = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Product"))


class NetworkServiceResponseBase(
        events.Stateful,
        components.Component,
    ):
    """A Network Service"""
    id = components.CharField()


class ExchangeLanNetworkServiceBase(
        crm.Ownable,
        NetworkServiceBase,
    ):
    """Exchange Lan Network Service"""
    name = components.CharField(
        max_length=40,
        help_text="""
            Exchange dependend service name, will be shown on the invoice.
        """)
    metro_area = components.CharField(
        max_length=3,
        help_text="""
            3 Letter (IATA) Airport Code of the MetroArea where
            the exchange lan network service is available.

            Example: FRA
        """)
    peeringdb_ixid = components.IntegerField(
        allow_null=True,
        required=False,
        help_text="""
            PeeringDB ixid
        """)
    ixfdb_ixid = components.IntegerField(
        allow_null=True,
        required=False,
        help_text="""
            id of ixfdb
        """)

    nsc_required_contact_roles = components.ListField(
        source="all_nsc_required_contact_roles",
        child=components.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified roles assigned to any associated contacts.

            The contacts can be associated through the `contacts` list
            property of the network service configuration.
        """)

    network_features = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkFeature"),
        many=True)

    ips = components.PrimaryKeyRelatedField(
        source="ip_addresses",
        queryset=qs.Any("ipam.IpAddress"),
        many=True,
        required=True)

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_EXCHANGE_LAN


class ExchangeLanNetworkService(
        NetworkServiceResponseBase,
        ExchangeLanNetworkServiceBase,
    ):
    """Exchange Lan Network Service"""


class ELineNetworkServiceBase(
        crm.Ownable,
        crm.Invoiceable,
        NetworkServiceBase,
    ):
    """E-Line Network Service"""
    name = components.CharField(
        max_length=40,
        help_text="""
            Name of the Private Vlan, will be shown on the invoice.
        """)

    joining_member_account = components.PrimaryKeyRelatedField(
        queryset=qs.Any("crm.Account"),
        help_text="""
            The account of the B-side member joining the E-Line.

            Example: "231829"
        """)

    capacity = components.IntegerField(
        min_value=1)

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_E_LINE


class ELineNetworkService(
        NetworkServiceResponseBase,
        ELineNetworkServiceBase,
    ):
    """E-Line Network Service"""
    nsc_required_contact_roles = components.ListField(
        source="all_nsc_required_contact_roles",
        child=components.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified roles assigned to any associated contacts.

            The contacts can be associated through the `contacts` list
            property of the network service configuration.
        """)


class ELineNetworkServiceRequest(ELineNetworkServiceBase):
    """E-Line Network Service Request"""


class ETreeNetworkServiceBase(
        crm.Ownable,
        crm.Invoiceable,
        NetworkServiceBase,
    ):
    """E-Tree Network Service"""
    public = components.BooleanField(default=False)

    name = components.CharField(
        max_length=40,
        help_text="""
            Name of the Private Vlan, will be shown on the invoice.
        """)
    initiator_capacity = components.IntegerField(
        min_value=1)

    default_capacity_min = components.IntegerField(
        min_value=1,
        allow_null=True)
    default_capacity_max = components.IntegerField(
        min_value=1,
        allow_null=True)

    ips = components.PrimaryKeyRelatedField(
        queryset=qs.Any("ipam.IpAddress"),
        source="ip_addresses",
        many=True,
        required=False)

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_E_TREE


class ETreeNetworkService(
        NetworkServiceResponseBase,
        ETreeNetworkServiceBase,
    ):
    """E-Tree Network Service"""
    nsc_required_contact_roles = components.ListField(
        source="all_nsc_required_contact_roles",
        child=components.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified roles assigned to any associated contacts.

            The contacts can be associated through the `contacts` list
            property of the network service configuration.
        """)
    network_features = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkFeature"),
        many=True)

    member_joining_rules = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.MemberJoiningRule"),
        many=True)



class ETreeNetworkServiceRequest(ETreeNetworkServiceBase):
    """E-Tree Network Service Request"""


class ELanNetworkServiceBase(
        crm.Ownable,
        crm.Invoiceable,
        NetworkServiceBase,
    ):
    """E-Line Network Service"""
    public = components.BooleanField(default=False)

    name = components.CharField(
        max_length=40,
        help_text="""
            Name of the Private Vlan, will be shown on the invoice.
        """)
    initiator_capacity = components.IntegerField(
        min_value=1)

    default_capacity_min = components.IntegerField(
        min_value=1,
        allow_null=True)
    default_capacity_max = components.IntegerField(
        min_value=1,
        allow_null=True)

    billing_account = components.PrimaryKeyRelatedField(
        queryset=qs.Any("crm.Account"))

    ips = components.PrimaryKeyRelatedField(
        queryset=qs.Any("ipam.IpAddress"),
        source="ip_addresses",
        many=True,
        required=False)

    __polymorphic_type__ = \
        NETWORK_SERVICE_TYPE_E_LAN


class ELanNetworkService(
        NetworkServiceResponseBase,
        ELanNetworkServiceBase,
    ):
    """E-Lan Network Service"""
    nsc_required_contact_roles = components.ListField(
        source="all_nsc_required_contact_roles",
        child=components.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified roles assigned to any associated contacts.

            The contacts can be associated through the `contacts` list
            property of the network service configuration.
        """)
    member_joining_rules = components.PrimaryKeyRelatedField(
       queryset=qs.Any("service.MemberJoiningRule"),
       many=True)

    network_features = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkFeature"),
        many=True)


class ELanNetworkServiceRequest(ELanNetworkServiceBase):
    """E-Lan Network Service Request"""


class NetworkService(components.PolymorphicComponent):
    """Polymorphic Network Services"""
    serializer_classes = {
        NETWORK_SERVICE_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkService,
        NETWORK_SERVICE_TYPE_E_LINE:
            ELineNetworkService,
        NETWORK_SERVICE_TYPE_E_TREE:
            ETreeNetworkService,
        NETWORK_SERVICE_TYPE_E_LAN:
            ELanNetworkService,
    }

    entity_types = {
        "service.ExchangeLanNetworkService":
            NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
        "service.ELineNetworkService":
            NETWORK_SERVICE_TYPE_E_LINE,
        "service.ELanNetworkService":
            NETWORK_SERVICE_TYPE_E_LAN,
        "service.ETreeNetworkService":
            NETWORK_SERVICE_TYPE_E_TREE,
    }


class NetworkServiceRequest(components.PolymorphicComponent):
    """Polymorphic Network Service Request"""
    serializer_classes = {
        NETWORK_SERVICE_TYPE_E_LINE:
            ELineNetworkServiceRequest,
        NETWORK_SERVICE_TYPE_E_TREE:
            ETreeNetworkServiceRequest,
        NETWORK_SERVICE_TYPE_E_LAN:
            ELanNetworkServiceRequest,
    }

    entity_types = {
        "service.ELineNetworkService":
            NETWORK_SERVICE_TYPE_E_LINE,
        "service.ELanNetworkService":
            NETWORK_SERVICE_TYPE_E_LAN,
        "service.ETreeNetworkService":
            NETWORK_SERVICE_TYPE_E_TREE,
    }


