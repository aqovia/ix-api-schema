

"""
Catalog Entities
----------------

This file describes platform entities like devices, facilities,
products, services...

"""

from ixapi_schema.coupling import qs
from ixapi_schema.openapi import components


# Constants
PRODUCT_TYPE_EXCHANGE_LAN = "exchange_lan"
PRODUCT_TYPE_CLOSED_USER_GROUP = "closed_user_group"
PRODUCT_TYPE_ELINE = "eline"
PRODUCT_TYPE_ELAN = "elan"
PRODUCT_TYPE_ETREE = "etree"
PRODUCT_TYPE_CLOUD = "cloud"

PRODUCT_TYPES = [
    PRODUCT_TYPE_EXCHANGE_LAN,
    PRODUCT_TYPE_CLOSED_USER_GROUP,
    PRODUCT_TYPE_ELINE,
    PRODUCT_TYPE_ELAN,
    PRODUCT_TYPE_ETREE,
    PRODUCT_TYPE_CLOUD,
]


class CloudProviderBase(components.Component):
    """Cloud Provider"""
    name = components.CharField(max_length=80)


class CloudProvider(CloudProviderBase):
    """Cloud Provider"""
    id = components.CharField(max_length=80)


class FacilityBase(components.Component):
    """Facility"""
    name = components.CharField(
        max_length=80,
        help_text="""
            Name of the Datacenter, how the DC calls itself.

            Example: Crater DC Moon 1
        """)
    metro_area = components.CharField(
        max_length=3,
        help_text="""
            3 Letter (IATA) Airport Code of the MetroArea where the DC is in.

            Example: FRA
        """)
    address_country = components.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE

            example: US
        """)
    address_locality = components.CharField(
        max_length=80,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    address_region = components.CharField(
        max_length=80,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = components.CharField(
        max_length=18,
        help_text="""
            A postal code. For example, 9404
            example: "9409"
        """)
    street_address = components.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)

    peeringdb_facility_id = components.IntegerField(
        allow_null=True,
        max_value=2147483647,
        min_value=0,
        required=False,
        help_text="""
            [PeeringDB](https://www.peeringdb.com) facitlity ID,
            can be extracted from the url https://www.peeringdb.com/fac/$id

            Example: 103
        """)

    organisation_name = components.CharField(
        max_length=80,
        source="operator_name",
        help_text="""
            Name of Datacenter operator
            Example: Moon Datacenters
        """)

    cluster = components.CharField(
        allow_null=True,
        allow_blank=True,
        required=False,
        max_length=80,
        source="cluster_name",
        help_text="""
            Group of facilities within the same building/campus
            Example: "60 Hudson, NYC"
        """)


class Facility(FacilityBase):
    """Facility"""
    id = components.CharField(max_length=80)


class DeviceCapability(components.Component):
    """Device Capability"""
    media_type = components.CharField(
        read_only=True,
        max_length=20,
        help_text="""
            The media type of the port (e.g. 1000BASE-LX, 10GBASE-LR, ...)

            Example: "1000BASE-LX"
        """)
    speed = components.IntegerField(
        read_only=True,
        help_text="""
            Speed of port in Mbit/s

            Example: 1000
        """)

    qinq_capable = components.BooleanField(
        read_only=True,
        source="q_in_q",
        help_text="""
            Ports supports inner vlan tag (QinQ)

            Example: true
        """)
    max_lag = components.IntegerField(
        read_only=True,
        max_value=32767,
        min_value=0,
        help_text="""
            Maximum count of ports which can be bundled to a max_lag
            Example: 8
        """)
    availability = components.IntegerField(
        read_only=True,
        source="availability_count",
        max_value=2147483647,
        min_value=0,
        help_text="""
            Count of available ports on device

            Example: 23
        """)


class DeviceBase(components.Component):
    """Device"""
    name = components.CharField(
        max_length=180,
        help_text="""
            Name of the device

            Example: edge2.moon.space-ix.net
        """)

    capabilities = DeviceCapability(
        many=True, read_only=True)

    physical_facility = components.PrimaryKeyRelatedField(
        source="physical_facility_id",
        read_only=True,
        help_text="""
            Identifier of the facilitiy where the device
            is physically based.
        """)

class Device(DeviceBase):
    """Device"""
    id = components.CharField(max_length=80)


class DeviceConnectionBase(components.Component):
    """Device Connection"""
    max_capacity = components.IntegerField(
        max_value=2147483647, min_value=0)
    device = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Device"))
    connected_device = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Device"))


class DeviceConnection(DeviceConnectionBase):
    """Device Connection"""
    id = components.CharField(max_length=80)


class PointOfPresenceBase(components.Component):
    """Point Of Presence"""
    name = components.CharField(max_length=40)
    physical_facility = components.PrimaryKeyRelatedField(
        read_only=True)

    reachable_facilities = components.ManyRelatedField(
        child_relation=components.PrimaryKeyRelatedField(
            queryset=qs.Any("catalog.Facility")))

    available_devices = components.PrimaryKeyRelatedField(
        source="available_physical_devices",
        queryset=qs.Any("catalog.Device"),
        many=True)


class PointOfPresence(PointOfPresenceBase):
    """Point Of Presence"""
    id = components.CharField(max_length=80)


class ProductBase(components.Component):
    """Product Base"""
    name = components.CharField(
        max_length=80,
        help_text="""
            Name of the product
        """)


    # available_devices = serializers.ManyRelatedField(
    #    source="devices",
    #    allow_empty=False,
    #    child_relation=serializers.PrimaryKeyRelatedField(
    #        allow_empty=False,
    #        queryset=Device.objects.all()))


class ExchangeLanNetworkProductBase(ProductBase):
    """Exchange Lan Network Product"""
    metro_area = components.CharField(
        allow_blank=True,
        allow_null=True,
        max_length=3,
        required=False,
        help_text="""
            The metro area the product is available

            Example: MUC
        """)

    __polymorphic_type__ = PRODUCT_TYPE_EXCHANGE_LAN


class ExchangeLanNetworkProduct(ExchangeLanNetworkProductBase):
    """Exchange Lan Network Product"""
    id = components.CharField(max_length=80)


class ClosedUserGroupNetworkProductBase(ProductBase):
    """Closed User Group Network Product"""
    __polymorphic_type__ = PRODUCT_TYPE_CLOSED_USER_GROUP


class ClosedUserGroupNetworkProduct(ClosedUserGroupNetworkProductBase):
    """Closed User Group"""
    id = components.CharField(max_length=80)


class ELineNetworkProductBase(ProductBase):
    """E-Line Network Product"""
    __polymorphic_type__ = PRODUCT_TYPE_ELINE


class ELineNetworkProduct(ELineNetworkProductBase):
    """E-Line Network Product"""
    id = components.CharField(max_length=80)


class ELanNetworkProductBase(ProductBase):
    """E-Lan Network Product"""
    __polymorphic_type__ = PRODUCT_TYPE_ELAN


class ELanNetworkProduct(ELanNetworkProductBase):
    """E-Lan Network Product"""
    id = components.CharField(max_length=80)


class ETreeNetworkProductBase(ProductBase):
    """E-Tree Network Product"""
    __polymorphic_type__ = PRODUCT_TYPE_ETREE


class ETreeNetworkProduct(ETreeNetworkProductBase):
    """E-Tree Network Product"""
    id = components.CharField(max_length=80)


class CloudNetworkProductBase(ProductBase):
    """Cloud Network Product"""
    zone = components.CharField(max_length=80)
    handover_point = components.CharField(max_length=80)

    provider = components.CharField(source="cloud_provider_name")

    __polymorphic_type__ = PRODUCT_TYPE_CLOUD


class CloudNetworkProduct(CloudNetworkProductBase):
    id = components.CharField(max_length=80)


class Product(components.PolymorphicComponent):
    """Polymorphic Product"""
    serializer_classes = {
        PRODUCT_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkProduct,
        PRODUCT_TYPE_ELINE:
            ELineNetworkProduct,
        PRODUCT_TYPE_ELAN:
            ELanNetworkProduct,
        PRODUCT_TYPE_ETREE:
            ETreeNetworkProduct,
    }

    entity_types = {
        "catalog.ExchangeLanNetworkProduct":
            PRODUCT_TYPE_EXCHANGE_LAN,
        "catalog.ClosedUserGroupNetworkProduct":
            PRODUCT_TYPE_CLOSED_USER_GROUP,
        "catalog.ELineNetworkProduct":
            PRODUCT_TYPE_ELINE,
        "catalog.ELanNetworkProduct":
            PRODUCT_TYPE_ELAN,
        "catalog.ETreeNetworkProduct":
            PRODUCT_TYPE_ETREE,
        "catalog.CloudNetworkProduct":
            PRODUCT_TYPE_CLOUD,
    }

