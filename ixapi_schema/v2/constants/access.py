
import enum

# FIXME: We can not compare v1 and v2 enums.
#        This is annoying.
from ixapi_schema.v1.constants.access import (
    ConnectionMode,
    BGPSessionType,
    RouteServerSessionMode,
    ConnectionMode,
    LACPTimeout,
)

class VLanEthertype(enum.Enum):
    E_0x8100 = "0x8100"
    E_0x88a8 = "0x88a8"
    E_0x9100 = "0x9100"


class VLanType(enum.Enum):
    PORT = "port"
    DOT1Q = "dot1q"
    QINQ = "qinq"

#
# Config Types
#
NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN = "exchange_lan"
NETWORK_SERVICE_CONFIG_TYPE_CLOUD = "cloud"
NETWORK_SERVICE_CONFIG_TYPE_E_LINE = "e_line"
NETWORK_SERVICE_CONFIG_TYPE_E_TREE = "e_tree"
NETWORK_SERVICE_CONFIG_TYPE_E_LAN = "e_lan"


NETWORK_SERVICE_CONFIG_TYPES = [
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_E_LINE,
    NETWORK_SERVICE_CONFIG_TYPE_E_TREE,
    NETWORK_SERVICE_CONFIG_TYPE_E_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
]

NETWORK_SERVICE_CONFIG_ENTITIES = {
    "access.ExchangeLanNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    "access.ELineNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_E_LINE,
    "access.ELanNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_E_LAN,
    "access.ETreeNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_E_TREE,
    "access.CloudNetworkServiceConfig":
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
}

NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING = "blackholing"
NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER = "route_server"
NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER = "ixp_router"

NETWORK_FEATURE_CONFIG_TYPES = [
    NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING,
    NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
    NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER,
]

NETWORK_FEATURE_CONFIG_ENTITIES = {
    "access.BlackholingNetworkFeatureConfig":
        NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING,
    "access.RouteServerNetworkFeatureConfig":
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
    "access.IXPRouterNetworkFeatureConfig":
        NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER,
}

