
import inspect

from rest_framework import serializers
from django.apps import apps

from ixapi_schema.openapi.components import utils
from ixapi_schema.v2.constants.access import VLanEthertype
from ixapi_schema.coupling import qs


def _resolve_model(model_name):
    """Resolve a model by name"""
    app_label, model = model_name.split(".", 2)
    return apps.get_model(app_label, model)


class Component(serializers.Serializer):
    pass


class PolymorphicComponentMeta(serializers.SerializerMetaclass):
    """
    Meta class for polymorphic components.
    """
    def __new__(cls, name, bases, attrs):
        """
        When creating the polymorphic serializer,
        resolve entity types.
        """
        entity_types = attrs.get("entity_types", {})
        entity_types = {_resolve_model(model): entity_type
                        for model, entity_type in entity_types.items()}
        attrs["entity_types"] = entity_types
        return super().__new__(cls, name, bases, attrs)


class PolymorphicComponent(Component, metaclass=PolymorphicComponentMeta):
    """
    Serializer for polymorphic models.

    Selects the fitting serializer based on a mapping,
    and adds the type to the result.
    """
    # Prevent serializer from showing up in the schema
    __serializer_base__ = True

    __polymorphic_on__ = "type"

    # Serializer mapping
    serializer_classes: dict = {}
    entity_types: dict = {}

    def __init__(self, *args, entity_type=None, **kwargs):
        """Override init to provide an optional type kwarg"""
        super().__init__(*args, **kwargs)

        # Map class to entity's type string representation
        if entity_type and not isinstance(entity_type, str):
            entity_type = self.entity_types[entity_type]

        self.entity_type = entity_type

    def get_entity(self, obj):
        """Get entity for object"""
        return None

    def to_representation(self, obj):
        """
        Serialize an entity. Select serializer based on model class.

        :param entity: The entity to serialize.
        """
        # Get polymorphic entity by resolve method.
        # if this fails, we try a dict based mapping.
        entity_class = self.get_entity(obj)
        if not entity_class:
            # Fall back to dict based mapping
            entity_type = self.entity_types.get(obj.__class__)
            if not entity_type:
                raise RuntimeError(
                    f"Invalid Type Mapping: {obj.__class__}")

            # Get polymorphic subentity
            entity_class = self.serializer_classes[entity_type]

        # Create Entity and serialize obj
        entity = entity_class(obj)
        data = entity.data

        # Add type to result
        data[self.__polymorphic_on__] = entity_class.__polymorphic_type__

        return data

    def to_internal_value(self, data):
        """
        Deserialize data with a matching entity deserializer.

        :param data: Incoming entity data
        """
        entity_type = data.get(self.__polymorphic_on__, self.entity_type)
        if not entity_type:
            raise serializers.ValidationError({
                "type": "type field missing"})

        # Get serializer
        serializer_class = self.serializer_classes.get(entity_type)
        if not serializer_class:
            raise serializers.ValidationError({
                "type": "invalid type given"})

        serializer = serializer_class(data=data, partial=self.partial)
        serializer.is_valid(raise_exception=True)

        # Include type in data
        data = serializer.validated_data
        data[self.__polymorphic_on__] = entity_type

        return data


class OuterVlanField(serializers.Field):
    """
    An outer vlan representation
    """
    def to_internal_value(self, data):
        """Decode outer vlan input"""
        if len(data) != 2:
            raise serializers.ValidationError("Outer Vlan must be a tuple")

        ethertype, number = data

        try:
            ethertype = VLanEthertype(ethertype)
        except Exception as e:
            raise serializers.ValidationError(
                "Invalid ethertype: {}".format(ethertype))

        try:
            number = int(number)
        except ValueError as e:
            raise serializers.ValidationError(
                "Invalid vlan number: {}".format(number))

        return {
            "ethertype": ethertype,
            "number": number,
        }

    def to_representation(self, entity):
        """
        Create representation from VLan entity
        """
        return (entity.ethertype.value, entity.number)


class NamedVlanField(serializers.Serializer):
    """
    A named vlan representation
    """
    def to_internal_value(self, data):
        """
        Decode a named vlan representation to our internal value
        """
        if isinstance(data, int):
            return {
                "name": None,
                "number": data,
            }

        if isinstance(data, list):
            try:
                number, name, *_ = data
                number = int(number)
            except:
                raise ValidationError("Invalid named vlan input")
            return {
                "name": name,
                "number": number,
            }

        raise ValidationError("Unexpected input for named vlan")


    def to_representation(self, entity):
        """
        Make representation from named vlan entity
        """
        if not entity.name:
            return entity.number
        else:
            return [entity.number, entity.name]


class PrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    """
    Subclass pk related field, to make the response compliant
    with our own OpenAPI spec: ID should be of type string.
    """
    def __init__(self, *args, queryset=None, **kwargs):
        """
        When initializing the related field,
        resolve a decoupled queryset into a real django model
        queryset.
        """
        if isinstance(queryset, qs.Any):
            # Resolve model queryset
            model = _resolve_model(queryset.model_name)
            queryset = model.objects.all()

        return super().__init__(*args, queryset=queryset, **kwargs)

    def to_representation(self, value):
        """Override representation"""
        res = super(PrimaryKeyRelatedField, self).to_representation(
            value)
        return str(res)


class EnumField(serializers.ChoiceField):
    """Serialize an enum field"""
    def __init__(self, enum_class, values={}, **kwargs):
        """
        Override initialization with fixed choice set
        You can provide a mapping of values:

        Consider:
            Speed(Enum):
                SPEED_1G = 1000

        A mapping would be:
            Speed.SPEED_1G: "1G"

        """
        self.enum_class = enum_class
        self.values = values
        self.inv_values = utils.reverse_mapping(values)

        # Choices from mapping
        choices = [(e.name, e.name) for e in enum_class] + \
                  [(e.value, e.name) for e in enum_class] + \
                  [(e.value, e.value) for e in enum_class] + \
                  [(name, name) for _, name in values.items()]

        super().__init__(choices, **kwargs)

    def to_internal_value(self, data):
        """Convert data to enum value"""
        value = self.inv_values.get(data)
        if not value:
            try:
                value = self.enum_class(data)
            except ValueError:
                try:
                    value = self.enum_class[data]

                except KeyError:
                    pass

            if value:
                return value

            # Try to fall back enum_class's values
            if isinstance(data, str):
                data = data.upper()

            try:
                value = self.enum_class(data)
            except ValueError:
                try:
                    value = self.enum_class[data]
                except KeyError:
                    # We did everything we could.
                    raise serializers.ValidationError("Unknown enum value")

        return value

    def to_representation(self, value):
        """Convert enum value to representation"""
        value_rep = self.values.get(value)
        if not value_rep:
            value_rep = value.value

        return value_rep

