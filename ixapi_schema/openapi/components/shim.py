

# Export rest framework components
from rest_framework import serializers
from rest_framework.serializers import *

from ixapi_schema.openapi.components import utils


class Component(serializers.Serializer):
    pass


class Field(serializers.Field):
    pass


class OuterVlanField(Field):
    pass

class NamedVlanField(Field):
    pass


class RelatedField:
    def __init__(
        self,
        queryset=None,
        child_relation=None,
        many=False,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.many = many
        self.queryset = queryset
        self.child_relation = child_relation

    def to_representation(self, obj):
        return obj


class PrimaryKeyRelatedField(RelatedField, Field):
    pass


class ManyRelatedField(RelatedField, Field):
    pass


class EnumField(serializers.ChoiceField):
    def __init__(self, enum_class, values={}, **kwargs):
        super().__init__(enum_class, **kwargs)
        self.enum_class = enum_class
        self.values = values
        self.inv_values = utils.reverse_mapping(values)


class ListField(Field):
    def __init__(self, *args, **kwargs):
        super().__init__(
            required=kwargs.get("required"))
        for k, v in kwargs.items():
            setattr(self, k, v)


class PolymorphicComponent(Component):
    pass

