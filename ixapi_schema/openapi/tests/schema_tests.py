
"""
Test OpenAPI Schema Generation
"""

from ixapi_schema import v1
from ixapi_schema.openapi import schema


def test_generate():
    """Test generating the schema"""
    doc = schema.generate(v1)
    assert doc

