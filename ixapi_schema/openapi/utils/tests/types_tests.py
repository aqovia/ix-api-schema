
from datetime import datetime

from ixapi_schema.openapi.utils import types


def test_is_json_serializable():
    """Test serializability check"""
    # The following types should be serializable
    assert types.is_json_serializable("Foo")
    assert types.is_json_serializable(42)
    assert types.is_json_serializable(2.3)
    assert types.is_json_serializable(True)

    assert not types.is_json_serializable(lambda x: x)
    assert not types.is_json_serializable(datetime.now())
