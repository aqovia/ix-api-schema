
"""
Test filter meta data generator
"""

from ixapi_schema.openapi.generators import filters_meta
from ixapi_schema.openapi.components import filters
from ixapi_schema.v1.entities import catalog


def test_choices_enum():
    """Test enumerating all choices in a choice field"""
    TYPE_CHOICES = (
        (catalog.PRODUCT_TYPE_EXCHANGE_LAN,
         catalog.PRODUCT_TYPE_EXCHANGE_LAN),
        (catalog.PRODUCT_TYPE_CLOSED_USER_GROUP,
         catalog.PRODUCT_TYPE_CLOSED_USER_GROUP),
        (catalog.PRODUCT_TYPE_ELINE,
         catalog.PRODUCT_TYPE_ELINE),
    )

    # Test with simple tuple choices
    choice_filter = filters.ChoiceFilter(
        method="filter_type",
        choices=TYPE_CHOICES)
    choices = choice_filter.extra['choices']
    choices_enum = filters_meta._choices_enum(choices)

    assert choices_enum == [
        catalog.PRODUCT_TYPE_EXCHANGE_LAN,
        catalog.PRODUCT_TYPE_CLOSED_USER_GROUP,
        catalog.PRODUCT_TYPE_ELINE]


def test_choice_enum_from_enum():
    """Test enumerating all enum choices"""
    some_filter = filters.CharFilter(choices=["foo", "bar"])
    choices = some_filter.extra['choices']
    choices_enum = filters_meta._choices_enum(choices)
    assert type(choices_enum[0]) == str


def test_frag_from_filter():
    """Test getting filter schema fragments"""
    all_filters = [
        ("id", filters.BulkIdFilter()),
        ("name", filters.CharFilter()),
        ("amount", filters.NumberFilter()),
        ("date", filters.DateTimeFilter()),
    ]


    for name, filter_field in all_filters:
        assert filters_meta.frag_from_filter(name, filter_field)
