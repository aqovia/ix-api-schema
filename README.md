
IX-API: Schema
==============


This project contains the source files for generating
the OpenAPI IX-API specs.

Components can be directly used for serialization.


## Usage

The `manage.py` serves as an entry point to the application.
You can choose to invoke it either locally via `./bin/manage` or
using Docker.

You can choose to either generate or to serve the API specs.

    manage.py generate_schema [version] [json/yaml] [pretty]
    manage.py export_problem_details [version] <output path>
    manage.py serve [127.0.0.1:9080]

### With Docker

    docker build . -t ix-api-schema:latest
    docker run -it --rm ix-api-schema:latest generate_schema v2 json
    docker run -it --rm -p 8080:8080 ix-api-schema:latest serve
    docker run -it --rm -v `pwd`/error-documents:/output ix-api-schema:latest export_problem_details /output

## Development

You can use a local development environment with `venv`.
To get you started, just run

    ./bin/venv_init

which will create a new virtual env and installs all
required dependencies.

To work with the application just run

    ./bin/manage



